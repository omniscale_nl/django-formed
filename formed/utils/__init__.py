# coding=utf-8
from formed import settings
from django.utils import six


def get_field_display_name(field_type):
    """
    Returns the display name for the field with the given field type.
    :param str field_type: The type of the field. For example 'CharField'.
    :return:
    """
    for field in six.itervalues(settings.FORMED_FORM_FIELDS):
        if field['type'] == field_type:
            return field['name']
    return None
